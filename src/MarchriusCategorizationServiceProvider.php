<?php

namespace Marchrius\Categorization;

use Illuminate\Support\ServiceProvider;

class MarchriusCategorizationServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes(array(
            __DIR__ . '/config/config.php' => config_path('marchrius-categorization.php'),
        ), 'config');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app['CategorizationManager'] = $this->app->share(function () {
            return new CategorizationManager(config('marchrius-categorization.file'));
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return string[]
     */
    public function provides()
    {
        return array(
            'CategorizationManager'
        );
    }
}