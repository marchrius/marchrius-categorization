<?php

namespace Marchrius\Categorization\Exception;

use Throwable;

class CategorizationRegExpException extends CategorizationException
{
    private static $_template = "Using keyword: %s";

    private $keyword;

    public function __construct($keyword = "", $code = 0, Throwable $previous = null)
    {
        $this->keyword = $keyword;

        parent::__construct(sprintf(self::$_template, $keyword), $code, $previous);
    }
}