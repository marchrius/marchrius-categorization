<?php

namespace Marchrius\Categorization\Facades;

use Illuminate\Support\Facades\Facade;

class CategorizationManager extends Facade
{

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'configurationManager';
    }
}