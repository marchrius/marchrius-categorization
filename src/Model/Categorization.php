<?php

namespace Marchrius\Categorization\Model;

use Marchrius\Categorization\CategorizationManager;
use Marchrius\Categorization\Exception\CategorizationRegExpException;

/**
 * Class Categorization
 * @package Marchrius\Categorization\Model
 *
 * @property  CategorizationManager $manager
 */
class Categorization
{
    /**
     * Categorization constructor.
     * @param CategorizationManager $manager
     */
    public function __construct(CategorizationManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param string ... strings
     * @return bool|false|int|string
     */
    public function guess()
    {
        $scored = call_user_func_array(array($this, 'scoring'), func_get_args());

        if ($this->manager->config->hasDefault)
            $guessed = $this->manager->config->default[$this->manager->config->property];
        else
            $guessed = false;

        foreach ($scored as $key => $score) {
            if (count($score) > 0) {
                $guessed = array_search(max($score), $score);
                break;
            }
        }

        return $guessed;
    }

    /**
     * @return array
     * @throws CategorizationRegExpException
     */
    public function scoring()
    {
        $arguments = array();

        // Reduce arguments, check for strictly-equal arguments not supported in key-value pair

        for ($i = 0; $i < func_num_args(); $i++) {
            $arg = func_get_arg($i);
            if (in_array($arg, $arguments))
                continue;
            $arguments[] = $arg;
        }

        $manager = $this->manager;
        $config = $manager->config;

        $categories = $config->categories;
        $property = $config->property;
        $key = $config->key;
        $delimiter = $config->delimiter;
        $asObject = $config->asObject;
        $default = $config->default;
        $hasDefault = $config->hasDefault;

        $scores = array();

        foreach ($arguments as $arg) {
            $scores[$arg] = array();
            foreach ($categories as $categoryKey => $category) {
                if ($asObject) {
                    $subKeys = explode($delimiter, $key);
                    foreach ($subKeys as $subKey) {
                        $foundMatch = $this->searchMatches($arg, $category[$subKey], true);
                        if ($foundMatch !== false && $foundMatch > 0) {
                            if (!isset($scores[$arg][$category[$property]]))
                                $scores[$arg][$category[$property]] = 0;
                            $scores[$arg][$category[$property]] += $foundMatch;
                        }
                    }
                } else {
                    $foundMatch = $this->searchMatches($arg, $category, true);
                    if ($foundMatch !== false && $foundMatch > 0) {
                        if (!isset($scores[$arg][$category[$property]]))
                            $scores[$arg][$category[$property]] = 0;
                        $scores[$arg][$category[$property]] += $foundMatch;
                    }
                }
            }
        }

        if ($hasDefault) {
            $empty = 0;
            foreach ($scores as $arg => $scoring) {
                if (count($scoring) == 0) {
                    $empty++;
                }
            }
            if ($empty == count($arguments)) {
                foreach ($scores as $arg => $scoring) {
                    $scores[$arg][$default[$property]] = 1;
                }
            }
        }
        return $scores;
    }

    /**
     * @internal
     *
     * @param string $string
     * @param array $keywords
     * @param boolean $scores
     * @param boolean $caseSensitive
     *
     * @return bool|integer
     *
     * @throws CategorizationRegExpException
     */
    function searchMatches($string, $keywords, $scores = false, $caseSensitive = false)
    {
        $regex = $caseSensitive ? '/\b%s\b/' : '/\b%s\b/i';
        $transform = $caseSensitive ? function ($a) {
            return $a;
        } : 'mb_strtolower';

        $finalScore = 0;
        foreach ($keywords as $keyword) {
            try {
                $result = preg_match_all(sprintf($regex, $keyword), $transform($string), $matches);
            } catch (\ErrorException $errorException) {
                throw new CategorizationRegExpException($keyword, 0, $errorException);
            }
            if ($result !== false)
                $finalScore += $result;
        }
        if ($scores)
            return $finalScore === false ? 0 : $finalScore;
        return !($finalScore === false || $finalScore === 0);
    }

}
