<?php

namespace Marchrius\Categorization\Model;

use Marchrius\Categorization\Exception\ConfigurationException;

/**
 * Class Configuration
 * @package Marchrius\Categorization\Model
 *
 * @property string $property The key to return to
 * @property string $key
 * @property string $delimiter
 * @property array $categories
 * @property boolean $asObject
 * @property boolean $hasDefault
 * @property mixed $default
 */
class Configuration
{

    public function __construct($config)
    {
        $_config = self::readConfiguration($config);

        $this->categories = isset($_config['categories']) ? $_config['categories'] : [];
        $this->property = isset($_config['property']) ? $_config['property'] : 'name';
        $this->key = isset($_config['key']) ? $_config['key'] : 'keywords';
        $this->delimiter = isset($_config['delimiter']) ? $_config['delimiter'] : '|';
        $this->asObject = isset($_config['as_object']) ? !!$_config['as_object'] : true;
        $this->default = isset($_config['default']) ? $_config['default'] : null;
        $this->hasDefault = isset($_config['default']);
    }

    /**
     *
     * @param array|\stdClass|string $config If string will be interpreted as configuration file
     *
     * @return array
     *
     * @throws ConfigurationException
     */
    private static function readConfiguration($config)
    {
        if (is_string($config)) {
            $contents = @file_get_contents($config);
            $_config = json_decode($contents, true);
        } else if (is_array($config)) {
            $_config = $config;
        } else if (is_object($config)) {
            $_config = json_decode(json_encode($config), true);
        } else {
            throw new ConfigurationException('$config argument must be array, object (stdClass) or a valid file path (absolute)');
        }

        if ($_config === false || is_null($_config)) {
            throw new ConfigurationException('CategorizationManager cannot understand your configuration file/array/object');
        }

        return $_config;
    }
}