<?php

namespace Marchrius\Categorization;

use Marchrius\Categorization\Model\Configuration;
use Marchrius\Categorization\Model\Categorization;

/**
 * Class CategorizationManager
 * @package Marchrius\Categorization
 *
 * @property Configuration $config
 */
class CategorizationManager
{

    /**
     * CategorizationManager constructor.
     * @param array|string $config
     */
    public function __construct($config)
    {
        $this->config = new Configuration($config);
    }

    /**
     * @param Configuration $config
     *
     * @return Configuration
     */
    public function config(Configuration $config = null)
    {
        if (is_null($config)) {
            return $this->config;
        }

        $this->config = $config;

        return $this->config;
    }

    /**
     * @param array|string $config
     * @return Categorization
     */
    public static function make($config = null)
    {
        $manager = new self($config);
        return new Categorization($manager);
    }

}