# Marchrius Categorization

Laravel package with PSR-0 support


## Installation

    composer require marchrius/categorization

Install service provider

```php
// config/app.php
'provider' => [
    ...
    ...
    Marchrius\Categorization\MarchriusCategorizationServiceProvider::class,
];
```

## Bootstrap

Publish the config file of this package with this command:

    php artisan vendor:publish --provider="Marchrius\Categorization\MarchriusCategorizationServiceProvider"
    
this will copy a start configuration file in your config directory.


## Usage

### Initialization of the library

```php
// Instantiate a categorization object
$categorizer = CategorizationManager::make(config('marchrius-categorization.file'));
```

### Guess category

Guess your category by keywords (case insensitive):
```php
$category = $categorizer->guess($string1, $string2, $string3, ..., $stringn);
```

Ouput:

```php
$category = "Cat1";
```

### Score category

Score category
```php
// Score category by
$category = $categorizer->scoring($string1, $string2, $string3, ..., $stringN);
```

Output:


```json
{
    "$string1": {
        "Cat1": 3,
        "Cat2": 2,
        "Cat3": 1,
        "CatN": 1
    },
    "$string2": {
        "Cat1": 5,
        "Cat2": 3,
        "Cat3": 2,
        "CatN": 1
    },
    "$string3": {
        "Cat1": 8,
        "Cat2": 4,
        "Cat3": 2,
        "CatN": 1
    },
    "$stringN": {
        "Cat1": 5,
        "Cat2": 4,
        "Cat3": 2,
        "CatN": 1
    }
}
```